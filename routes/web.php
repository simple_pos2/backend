<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('product')->group(function () {
    Route::get("/", [App\Http\Controllers\ProductsController::class, 'get']);
    Route::post("/create", [App\Http\Controllers\ProductsController::class, 'create']);
});


Route::prefix('order')->group(function () {
    Route::get("/", [App\Http\Controllers\OrdersController::class, 'get']);
    Route::post("/create", [App\Http\Controllers\OrdersController::class, 'create']);
});


Route::prefix('order-detail')->group(function () {
    Route::get("/", [App\Http\Controllers\OrderDetailsController::class, 'get']);
    Route::post("/create", [App\Http\Controllers\OrderDetailsController::class, 'create']);
});
