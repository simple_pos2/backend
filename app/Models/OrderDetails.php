<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    use HasFactory;

    protected $table = 'order_details';

    protected $fillable = [
        'money_payment',
        'money_back_payment'
    ];

    public function orders()
    {
        return $this->hasMany(Orders::class, 'order_details_id', 'id');
    }
}
