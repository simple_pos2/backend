<?php

namespace App\Builder;

use App\Dto\ProductsDto;

class ProductsBuilder
{
    public string $name = '';
    public string $image = '';
    public float $price = 0.0;


    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }


    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function build(): ProductsDto
    {
        return new ProductsDto(
            $this->name,
            $this->image,
            $this->price,
        );
    }
}
