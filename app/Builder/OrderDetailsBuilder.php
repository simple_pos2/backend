<?php

namespace App\Builder;

use App\Dto\OrderDetailsDto;

class OrderDetailsBuilder
{
    public float $money_payment = 0.0;
    public float $money_back_payment = 0.0;


    public function setMoneyPayment($money_payment)
    {
        $this->money_payment = $money_payment;
        return $this;
    }

    public function setMoneyBackPayment($money_back_payment)
    {
        $this->money_back_payment = $money_back_payment;
        return $this;
    }

    public function build(): OrderDetailsDto
    {
        return new OrderDetailsDto(
            $this->money_payment,
            $this->money_back_payment,
        );
    }
}
