<?php

namespace App\Builder;

use App\Dto\OrdersDto;

class OrdersBuilder
{
    public int $order_details_id = 0;
    public int $product_id = 0;
    public int $quantity = 0;


    public function setOrderDetailsId($order_details_id)
    {
        $this->order_details_id = $order_details_id;
        return $this;
    }

    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
        return $this;
    }


    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function build(): OrdersDto
    {
        return new OrdersDto(
            $this->order_details_id,
            $this->product_id,
            $this->quantity,
        );
    }
}
