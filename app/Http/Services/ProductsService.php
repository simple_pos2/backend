<?php

namespace App\Http\Services;

use App\Dto\ProductsDto;
use App\Http\Repositories\ProductsRepository;

class ProductsService
{
    private $productRepository;

    public function __construct(ProductsRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }


    public function get(int $limit, string $order_by, string $sort)
    {

        $data = $this->productRepository->get($limit, $order_by, $sort);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }

    public function create(ProductsDto $productsDto)
    {

        $data = $this->productRepository->create((array)$productsDto);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }
}
