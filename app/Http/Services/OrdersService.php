<?php

namespace App\Http\Services;

use App\Dto\OrdersDto;
use App\Http\Repositories\OrdersRepository;

class OrdersService
{
    private $ordersRepository;

    public function __construct(OrdersRepository $ordersRepository)
    {
        $this->ordersRepository = $ordersRepository;
    }


    public function get(int $limit, string $order_by, string $sort)
    {

        $data = $this->ordersRepository->get($limit, $order_by, $sort);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }

    public function create(OrdersDto $ordersDto)
    {

        $data = $this->ordersRepository->create((array)$ordersDto);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }
}
