<?php

namespace App\Http\Services;

use App\Dto\OrderDetailsDto;
use App\Http\Repositories\OrderDetailsRepository;
use App\Http\Repositories\OrdersRepository;

class OrderDetailsService
{
    private $orderDetailsRepository;
    private $ordersRepository;


    public function __construct(OrderDetailsRepository $orderDetailsRepository, OrdersRepository $ordersRepository)
    {
        $this->orderDetailsRepository = $orderDetailsRepository;
        $this->ordersRepository = $ordersRepository;
    }


    public function get(int $limit, string $order_by, string $sort)
    {

        $data = $this->orderDetailsRepository->get($limit, $order_by, $sort);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }

    public function create($products, OrderDetailsDto $orderDetailsDto)
    {

        $currentTime = now();

        $data = $this->orderDetailsRepository->create((array)$orderDetailsDto);

        $order_detail_id = 0;
        $data_orders = [];

        if (is_object($data)) {
            $order_detail_id = $data->id;
        }

        foreach ($products as &$product) {

            $order = [
                'order_details_id' => $order_detail_id,
                'product_id' => $product['id'],
                'quantity' => $product['qty'],
                'created_at' => $currentTime,
                'updated_at' => $currentTime,
            ];


            $data_orders[] = $order;
        }

        $this->ordersRepository->bulkCreate($data_orders);

        return ['status' => 'success', 'result' => ['data' => $data]];
    }
}
