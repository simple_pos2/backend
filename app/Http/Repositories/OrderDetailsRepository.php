<?php

namespace App\Http\Repositories;

use App\Models\OrderDetails;

class OrderDetailsRepository
{
    private $ordersDetailsModel;

    public function __construct(OrderDetails $ordersDetailsModel)
    {
        $this->ordersDetailsModel = $ordersDetailsModel;
    }

    public function get($limit, $order_by, $sort)
    {
        $rows = [];

        if ($limit) {
            $rows = $this->ordersDetailsModel::orderBy($order_by, $sort)->limit($limit)->get();
        } else {
            $rows = $this->ordersDetailsModel::orderBy($order_by, $sort)->get();
        }

        return $rows;
    }

    public function create($data)
    {

        $rows = $this->ordersDetailsModel::create($data);

        return $rows;
    }

    public function bulkCreate($data)
    {

        $rows = $this->ordersDetailsModel::insert($data);

        return $rows;
    }
}
