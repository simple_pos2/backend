<?php

namespace App\Http\Repositories;

use App\Models\Products;

class ProductsRepository
{
    private $productsModel;

    public function __construct(Products $productsModel)
    {
        $this->productsModel = $productsModel;
    }

    public function get($limit, $order_by, $sort)
    {
        $rows = [];

        if ($limit) {
            $rows = $this->productsModel::orderBy($order_by, $sort)->limit($limit)->get();
        } else {
            $rows = $this->productsModel::orderBy($order_by, $sort)->get();
        }

        return $rows;
    }

    public function create($data)
    {
        $rows = $this->productsModel::create($data);

        return $rows;
    }
}
