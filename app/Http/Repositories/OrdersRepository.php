<?php

namespace App\Http\Repositories;

use App\Models\Orders;

class OrdersRepository
{
    private $ordersModel;

    public function __construct(Orders $ordersModel)
    {
        $this->ordersModel = $ordersModel;
    }

    public function get($limit, $order_by, $sort)
    {
        $rows = $this->ordersModel::orderBy($order_by, $sort)->limit($limit)->get();

        return $rows;
    }

    public function create($data)
    {
        $rows = $this->ordersModel::create($data);

        return $rows;
    }

    public function bulkCreate($data)
    {
        $rows = $this->ordersModel::insert($data);

        return $rows;
    }
}
