<?php

namespace App\Http\Controllers;

use App\Builder\OrdersBuilder;
use App\Http\Services\OrdersService;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    private $ordersService;

    public function __construct(OrdersService $ordersService)
    {
        $this->ordersService = $ordersService;
    }


    public function get(Request $request)
    {
        $response = $this->ordersService->get(
            $request->query('limit', 5),
            $request->query('order_by', 'id'),
            $request->query('sort', 'ASC')
        );

        return response()->json($response, 200);
    }

    public function create(Request $request)
    {
        ['order_details_id' => $order_details_id, 'product_id' => $product_id, 'quantity' => $quantity] = $request->validate(['order_details_id' => 'required', 'product_id' => 'required', "quantity" => "required"]);

        $orderDetailsDto = (new OrdersBuilder())
            ->setOrderDetailsId($order_details_id)
            ->setProductId($product_id)
            ->setQuantity($quantity)
            ->build();

        $response = $this->ordersService->create($orderDetailsDto);


        return response()->json($response, 200);
    }
}
