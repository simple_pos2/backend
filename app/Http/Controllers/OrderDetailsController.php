<?php

namespace App\Http\Controllers;

use App\Builder\OrderDetailsBuilder;
use App\Http\Services\OrderDetailsService;
use Illuminate\Http\Request;

class OrderDetailsController extends Controller
{
    private $orderDetailsService;

    public function __construct(OrderDetailsService $orderDetailsService)
    {
        $this->orderDetailsService = $orderDetailsService;
    }


    public function get(Request $request)
    {
        $response = $this->orderDetailsService->get(
            $request->query('limit', 0),
            $request->query('order_by', 'id'),
            $request->query('sort', 'ASC')
        );

        return response()->json($response, 200);
    }

    public function create(Request $request)
    {
        ['products' => $products, 'money_payment' => $money_payment, 'money_back_payment' => $money_back_payment] = $request->validate(['products' => 'required', 'money_payment' => 'required', 'money_back_payment' => 'required']);
        
        $orderDetailsDto = (new OrderDetailsBuilder())
            ->setMoneyPayment((float)$money_payment)
            ->setMoneyBackPayment((float)$money_back_payment)
            ->build();


        $response = $this->orderDetailsService->create($products, $orderDetailsDto);


        return response()->json($response, 200);
    }
}
