<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Builder\ProductsBuilder;
use App\Http\Services\ProductsService;

class ProductsController extends Controller
{
    private $productsService;

    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
    }


    public function get(Request $request)
    {
        $response = $this->productsService->get(
            $request->query('limit', 0),
            $request->query('order_by', 'id'),
            $request->query('sort', 'ASC')
        );

        return response()->json($response, 200);
    }

    public function create(Request $request)
    {
        $image = $request->file('image');
        ['name' => $name, 'price' => $price] = $request->validate(['name' => 'required', 'price' => 'required', 'image' => 'image|mimes:jpeg,png,jpg|max:2048']);

        $image_name = 'image_product' . '-' . Str::uuid() . '.' . $image->getClientOriginalExtension();
        $image->storePubliclyAs("products", $image_name, "public");

        $productsDto = (new ProductsBuilder())
            ->setName($name)
            ->setImage($image_name)
            ->setPrice($price)
            ->build();

        $response = $this->productsService->create($productsDto);


        return response()->json($response, 200);
    }
}
