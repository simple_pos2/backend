<?php

namespace App\Dto;

class OrdersDto
{
    public int $order_details_id;
    public int $product_id;
    public int $quantity;

    public function __construct(int $order_details_id, int $product_id, int $quantity)
    {
        $this->order_details_id = $order_details_id;
        $this->product_id = $product_id;
        $this->quantity = $quantity;
    }
}
