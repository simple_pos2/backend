<?php

namespace App\Dto;

class ProductsDto
{
    public string $name;
    public string $image;
    public float $price;

    public function __construct(string $name, string $image, float $price)
    {
        $this->name = $name;
        $this->image = $image;
        $this->price = $price;
    }
}
