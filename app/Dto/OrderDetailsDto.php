<?php

namespace App\Dto;

class OrderDetailsDto
{
    public float $money_payment;
    public float $money_back_payment;

    public function __construct(float $money_payment, float $money_back_payment)
    {
        $this->money_payment = $money_payment;
        $this->money_back_payment = $money_back_payment;
    }
}
